#!/bin/bash

i=$1

lines1=`wc -l < bigdata/trip_data_$i.csv`
lines2=`wc -l < bigdata/trip_fare_$i.csv`
if [ $lines1 -eq $lines2 ]
then
	echo "bigdata/trip_data_$i.csv bigdata/trip_fare_$i.csv"
	cut -d ',' -f1 -f2 -f4 bigdata/trip_fare_$i.csv > fare_$i.csv
	cut -d ',' -f1 -f2 -f6 bigdata/trip_data_$i.csv > trip_$i.csv
	if diff -w fare_$i.csv trip_$i.csv >/dev/null ; then
		echo $i
		cut -d ',' -f10 -f11 bigdata/trip_fare_$i.csv > tot_$i.csv
		cut -d ',' -f9 bigdata/trip_data_$i.csv > tmp_$i.csv
		cat tot_$i.csv | paste -d, tmp_$i.csv - > data_$i.csv
		# paste -d, <(cat smalldata/trip_fare_0.csv | cut -d ',' -f10 -f11) <(cat smalldata/trip_data_0.csv | cut -d ',' -f9)
	else
	 	echo Different $i
	 	exit 1
	fi
	rm tot_$i.csv tmp_$i.csv
else
	echo Different $i
	exit 1
fi 
rm fare_$i.csv trip_$i.csv