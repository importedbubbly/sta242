#!/bin/bash

cat $( ls -1 data_1.csv | sort -n) > combine.csv
tail -n+2  $( ls -1 data_[2-9].csv | sort -n) >> combine.csv
tail -n+2 $( ls -1 data_[0-9][0-9].csv | sort -n ) >> combine.csv

sed '/data/d' combine.csv > data.csv

rm combine.csv
