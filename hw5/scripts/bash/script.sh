#!/bin/bash

count=1
echo "elapsed_ms, elapsed_ms_parallel" > elapsed.csv

for (( i=1; i<=$count; i++ )); do
	# elapsed_ms=`(time -p ./timeSeq.sh) 2>&1 > /dev/null |grep real | awk '{print $2}'`

	echo "starting paral $i"

	elapsed_ms_parallel=`./timeParallel.sh 2>&1 > /dev/null |grep real | awk '{print $2}' | tail -1`

	echo "$elapsed_ms, $elapsed_ms_parallel" >> elapsed.csv
done