CREATE EXTERNAL TABLE faredata
(
id STRUCT<oid:STRING, bsontype:INT>, 
     medallion STRING,
     hack_license STRING,
     vendor_id STRING,
     pickup_datetime STRING,
     payment_type STRING,
     fare_amount DOUBLE,
     surcharge DOUBLE,
     mta_tax DOUBLE,
     tip_amount DOUBLE,
     tolls_amount DOUBLE,
     total_amount DOUBLE     
)
STORED BY 'com.mongodb.hadoop.hive.MongoStorageHandler'
WITH SERDEPROPERTIES('mongo.columns.mapping'='{"id":"_id", "medallion":"medallion","hack_license":"hack_license",
"vendor_id":"vendor_id","pickup_datetime":"pickup_datetime",
"payment_type":"payment_type","fare_amount":"fare_amount",
"surcharge":"surcharge","mta_tax":"mta_tax","tip_amount":"tip_amount",
 "tolls_amount":"tolls_amount","total_amount":"total_amount"}')
TBLPROPERTIES('mongo.uri'='mongodb://localhost:27017/mongo-hadoop.fare'); 


