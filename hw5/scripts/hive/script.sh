#!/bin/bash
TRIPFILES="../../smalldata/trip/*"
FAREFILES="../../smalldata/fare/*"

echo "timediff" > timediff.csv

#count=10
#for i in $count
#do 
	for f in $TRIPFILES
	do
		mongoimport -d mongo-hadoop -c trip --type csv --file "$f" --headerline
	done > cpu.txt 

	for f in $FAREFILES
	do
		mongoimport -d mongo-hadoop -c fare --type csv --file "$f" --headerline
	done >> cpu.txt

	./extract.sh cpu.txt
#done
