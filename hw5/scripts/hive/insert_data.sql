set hive.auto.convert.join=false;
INSERT INTO TABLE data
SELECT 
f.id,
f.tolls_amount, 
f.total_amount, 
t.trip_time_in_secs
FROM faredata as f 
join tripdata as t
ON f.medallion = t.medallion
AND f.hack_license = t.hack_license
AND f.pickup_datetime = t.pickup_datetime;


