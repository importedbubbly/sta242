CREATE TABLE data
(
     id STRUCT<oid:STRING, bsontype:INT>,  
     trip_time_in_secs INT,
     tolls_amount DOUBLE,
     total_amount DOUBLE,
     diff DOUBLE
);

INSERT INTO data
SELECT
id,
trip_time_in_secs,
tolls_amount,
total_amount,
(total_amount-tolls_amount) as diff
from read
order by diff;



