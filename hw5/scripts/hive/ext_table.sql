CREATE EXTERNAL TABLE tripdata
(
id STRUCT<oid:STRING, bsontype:INT>, 
     medallion STRING,
     hack_license STRING,
     vendor_id STRING,
     rate_code STRING,
     store_and_fwd_flag STRING,
     pickup_datetime STRING,
     dropoff_datetime STRING,
     passenger_count INT,
     trip_time_in_secs DOUBLE,
     trip_distance DOUBLE,
     pickup_longitude STRING,
     pickup_latitude STRING,
     dropoff_longitude STRING,
     dropoff_latitude STRING
)
STORED BY 'com.mongodb.hadoop.hive.MongoStorageHandler'
WITH SERDEPROPERTIES('mongo.columns.mapping'='{"id":"_id", "medallion":"medallion","hack_license":"hack_license",
"vendor_id":"vendor_id","rate_code":"rate_code",
"store_and_forward_flag":"store_and_forward_flag",
"pickup_datetime":"pickup_datetime",
"dropoff_datetime":"dropoff_datetime",
"passenger_count":"passenger_count",
"trip_time_in_secs":"trip_time_in_secs",
"trip_distance":"trip_distance",
"pickup_longitude":"pickup_longitude",
"pickup_latitude":"pickup_latitude",
"dropoff_longitude":"dropoff_longitude",
"dropoff_latitude":"dropoff_latitude"}')
TBLPROPERTIES('mongo.uri'='mongodb://localhost:27017/mongo-hadoop.trip'); 


