CREATE TABLE trip
(
     id STRUCT<oid:STRING, bsontype:INT>, 
     medallion STRING,
     hack_license STRING,
     pickup_datetime STRING
);

CREATE TABLE fare
(
     id STRUCT<oid:STRING, bsontype:INT>, 
     medallion STRING,
     hack_license STRING,
     pickup_datetime STRING
);

CREATE TABLE read
(
     id STRUCT<oid:STRING, bsontype:INT>,  
     trip_time_in_secs INT,
     tolls_amount DOUBLE,
     total_amount DOUBLE
);



