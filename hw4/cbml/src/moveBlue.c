#include <R.h>
#include <Rinternals.h>

SEXP moveBlue(SEXP RinMatrix){
  int i, j, I, J;
  SEXP Rval, Rdim, Rcolors, Rvelocity, Rresult;

  /*Get the dimensions of the input matrix*/
  Rdim = getAttrib(RinMatrix, R_DimSymbol);
  I = INTEGER(Rdim)[0];
  J = INTEGER(Rdim)[1];

  RinMatrix = coerceVector(RinMatrix, REALSXP);

  /*Create the return matrix*/
  PROTECT(Rval = allocMatrix(REALSXP, I, J));
  PROTECT(Rcolors = allocVector(REALSXP,2));
  PROTECT(Rvelocity = allocVector(REALSXP,1));
  PROTECT(Rresult = allocVector(VECSXP, 2));
  REAL(Rcolors)[0] = 0; //white
  REAL(Rcolors)[1] = 2; //blue

  int ctr = 0;
  for (i = 0; i < I; i++)
    for (j = 0; j < J; j++)
    {
      if( REAL(RinMatrix)[i + I * j]==REAL(Rcolors)[1]) 
      {
        ctr++;
      }
      REAL(Rval)[i + I * j] = REAL(RinMatrix)[i + I * j];
    }

  int rpos [ctr];
  int cpos [ctr];
  int ind = 0;
  for (i = 0; i < I; i++)
    for (j = 0; j < J; j++)
    {
      if( REAL(RinMatrix)[i + I * j]==REAL(Rcolors)[1]) 
      {
        rpos[ind]=i;
        cpos[ind]=j;
        ind++;
      }
    }

  int changeToBlue;
  REAL(Rvelocity)[0] = 0;
  for(i = 0; i < ctr; i++) {
    changeToBlue = rpos[i] + 1;
    if(changeToBlue >= I)
      changeToBlue = 0;
    if(REAL(Rval)[changeToBlue + I * cpos[i]] == 0) {
      REAL(Rval)[changeToBlue + I * cpos[i]] = 2; 
      REAL(Rval)[rpos[i] + I * cpos[i]] = 0; 
      REAL(Rvelocity)[0] = REAL(Rvelocity)[0]+1;
    }
  }

  REAL(Rvelocity)[0] = REAL(Rvelocity)[0]/ctr;

  SET_VECTOR_ELT(Rresult, 0, Rval);
  SET_VECTOR_ELT(Rresult, 1, Rvelocity);
  UNPROTECT(4);

  return Rresult;
}