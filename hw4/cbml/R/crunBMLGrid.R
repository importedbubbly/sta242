crunBMLGrid <-
function(g,numSteps) {
  nsteps <- 0
#   par(ask = TRUE) #Allows user to look at each transition iteratively
  while(nsteps < numSteps) {
    if(is.even(nsteps)) {
      g <- cmoveRed(g)
    } else {
      g <- cmoveBlue(g)
    }
#     plot.bml(g$grid) #Use this when asking user
    nsteps <- nsteps + 1
  }
  nsteps <- 0
#     par(ask=FALSE) #Turns this function off
  plot.bml(g$grid) #Use this when we only care about the result
  return(g)
}
