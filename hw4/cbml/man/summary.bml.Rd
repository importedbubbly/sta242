\name{summary.bml}
\alias{summary.bml}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
reports the number of red cars, blue cars, number of total cells, red car velocity, blue car velocity
}
\description{
%%  ~~ A concise (1-5 lines) description of what the function does. ~~
}
\usage{
summary.bml(g)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{g}{
%%     ~~Describe \code{g} here~~
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
%%  ~Describe the value returned
%%  If it is a LIST, use
%%  \item{comp1 }{Description of 'comp1'}
%%  \item{comp2 }{Description of 'comp2'}
%% ...
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Sharon Gong
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (g) 
{
    cat("\nNumber of columns: ", ncol(g$grid))
    cat("\nNumber of rows: ", nrow(g$grid))
    cat("\nNumber of blue cars: ", g$nbluecars)
    cat("\nNumber of red cars: ", g$nredcars)
    cat("\nVelocity of red cars: ", g$rmoved)
    cat("\nVelocity of blue cars: ", g$bmoved)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
