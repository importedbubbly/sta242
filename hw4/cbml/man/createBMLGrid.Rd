\name{createBMLGrid}
\alias{createBMLGrid}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
creates grid
}
\description{
creates the grid of randomly placed red and blue cars
}
\usage{
createBMLGrid(r, c, ncars)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{r}{
number of rows
}
  \item{c}{
number of columns
}
  \item{ncars}{
ncars = [nredcars nbluecars]
nredcars: number of red cars
nbluecars: number of blue cars
}
}
\details{

}
\value{
\item{matrix }{Returns an object with a grid and classes including matrix, grid, bml}
}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Sharon Gong
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (r, c, ncars) 
{
    ncells <- r * c
    nredcars <- ncars[1]
    nbluecars <- ncars[2]
    mydata <- c(1:ncells)
    color <- rep(0, ncells)
    rpos <- sample(mydata, nredcars, replace = F)
    color[rpos] = 1
    newdata <- mydata[-rpos]
    bpos <- sample(newdata, nbluecars, replace = F)
    color[bpos] = 2
    grid <- matrix(data = color, nrow = r, ncol = c, byrow = TRUE)
    structure(list(grid = grid, rmoved = 0, bmoved = 0, nbluecars = nbluecars, 
        nredcars = nredcars), class = c(class(grid), "grid", 
        "bml"))
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
