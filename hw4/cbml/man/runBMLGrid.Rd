\name{runBMLGrid}
\alias{runBMLGrid}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{
rns the transitions
}
\description{
given a number of iterations for cor to move, this function calls moveBlue and moveRed to push the cars along
}
\usage{
runBMLGrid(g, numSteps)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{g}{
return object from moveBlue or moveRed
}
  \item{numSteps}{
number of iterations to see movement
}
}
\details{
%%  ~~ If necessary, more details than the description above ~~
}
\value{
\item{grid }{new gird with moved positions}

}
\references{
%% ~put references to the literature/web site here ~
}
\author{
Sharon Gong
}
\note{
%%  ~~further notes~~
}

%% ~Make other sections like Warning with \section{Warning }{....} ~

\seealso{
%% ~~objects to See Also as \code{\link{help}}, ~~~
}
\examples{
##---- Should be DIRECTLY executable !! ----
##-- ==>  Define data, use random,
##--	or do  help(data=index)  for the standard data sets.

## The function is currently defined as
function (g, numSteps) 
{
    nsteps <- 0
    while (nsteps < numSteps) {
        if (is.even(nsteps)) {
            g <- moveRed(g)
        }
        else {
            g <- moveBlue(g)
        }
        nsteps <- nsteps + 1
    }
    nsteps <- 0
    plot.bml(g$grid)
    return(g)
  }
}
% Add one or more standard keywords, see file 'KEYWORDS' in the
% R documentation directory.
\keyword{ ~kwd1 }
\keyword{ ~kwd2 }% __ONLY ONE__ keyword per line
