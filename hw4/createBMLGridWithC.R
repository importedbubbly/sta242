createDensityGrid <- function(r, c, p) {
  ncells <- r*c
  mydata <- c(1:ncells)
  taken <- c(1:(p*r*c))
  color <- rep(0,ncells)
  
  nredcars <- floor(length(taken)/2)
  nbluecars <- floor(length(taken)/2)
  
  rpos <- sample(mydata,nredcars,replace=F)
  color[rpos] <- 1
  newdata <- mydata[-rpos]
  bpos <- sample(newdata,nbluecars,replace=F)
  color[bpos]=2
  
  grid <- matrix(data=color,nrow=r,ncol=c,byrow=TRUE)
  structure(list(grid = grid, p=p,rmoved=0,bmoved=0,nbluecars=nbluecars,nredcars=nredcars), class = c(class(grid), "grid","bml"))
}

createBMLGrid <- function(r, c, ncars) {
  ncells <- r*c;
  nredcars <- ncars[1];
  nbluecars <- ncars[2];
  
  mydata <- c(1:ncells)
  color <- rep(0, ncells);
  
  rpos <- sample(mydata, nredcars, replace = F)
  color[rpos]=1
  newdata <- mydata[-rpos]
  bpos <- sample(newdata, nbluecars, replace = F)
  color[bpos]=2
  
  grid <- matrix(data = color, nrow = r, ncol = c, byrow=TRUE)
  
  structure(list(grid = grid,rmoved=0,bmoved=0,nbluecars=nbluecars,nredcars=nredcars), class = c(class(grid), "grid","bml"))
}

plot.bml <- function(grid) {
  rgrid <- reshape.bml(grid)
  image(rgrid, axes = FALSE, col = c("white", "red", "blue"))
}

reshape.bml <- function(grid) {
  tmp <- grid
  n <- nrow(grid)
  row <- c(1:n)
  
  tmp <- sapply(row, function(row) {
    tmp[row,] <- grid[n-row+1,]
  })
  
  grid <- tmp
}

cmoveBlue <- function(g) {
  if(!is.loaded('moveBlue'))
    dyn.load('moveBlue.so')
  result <- .Call("moveBlue",g$grid)
  g$grid <- result[[1]]
  g$bmoved <- result[[2]]
  return(g)
}

cmoveRed <- function(g) {
  if(!is.loaded('moveRed'))
    dyn.load('moveRed.so')
  result <- .Call("moveRed",g$grid)
  g$grid <- result[[1]]
  g$rmoved <- result[[2]]
  return(g)
}

moveRed <- function(g) {
  rind <- which(g$grid==1, arr.ind=TRUE) #find all reds
  rendInd <- rind
  rnewpos <- rind[,2]+1 #move all reds 1 to the right
  rnewpos[rnewpos > ncol(g$grid)] <- 1  #wrap around 
  rendInd[,2] <- rnewpos 
  
  i <- rind[,1]
  j <- rind[,2]
  ri <- rendInd[,1]
  rj <- rendInd[,2]
  
  coord <- cbind(i,j) 
  rcoord <- cbind(ri,rj)
  
  changeToWhite <- subset(coord, g$grid[rcoord]==0) #find corresponding values where next pos is white
  changeToRed <- subset(rcoord, g$grid[rcoord]==0)
  
  g$rmoved <- nrow(changeToRed)/nrow(rind) #calculates velocity
  
  g$grid[changeToWhite] = 0 #white
  g$grid[changeToRed] = 1 #red
  return(g)
}

moveBlue <- function(g) {
  ind <- which(g$grid==2, arr.ind=TRUE) #finds all pos where blue
  endInd <- ind
  newpos <- ind[,1]+1 #increment to next pos
  newpos[newpos > nrow(g$grid)] <- 1 
  endInd[,1] <- newpos
  
  i <- ind[,1]
  j <- ind[,2]
  bi <- endInd[,1]
  bj <- endInd[,2]
  
  coord <- cbind(i,j)
  bcoord <- cbind(bi,bj)
  
  changeToWhite <- subset(coord, g$grid[bcoord]==0) #find corresponding values where next pos is white
  changeToBlue <- subset(bcoord, g$grid[bcoord]==0)
  
  g$bmoved <- nrow(changeToBlue)/nrow(ind) #calculates velocity
  
  g$grid[changeToWhite] = 0 #white
  g$grid[changeToBlue] = 2 #blue
  
  return(g)
}

is.even <- function(num) {
  num %% 2 == 0
}

runBMLGrid <- function(g,numSteps) {
  nsteps <- 0
#     par(ask = TRUE) #Allows user to look at each transition iteratively
  while(nsteps < numSteps) {
    if(is.even(nsteps)) {
      g <- moveRed(g)
    } else {
      g <- moveBlue(g)
    }
#         plot.bml(g$grid) #Use this when asking user
    nsteps <- nsteps + 1
  }
  nsteps <- 0
#       par(ask=FALSE) #Turns this function off
  plot.bml(g$grid) #Use this when we only care about the result
  return(g)
}

crunBMLGrid <- function(g,numSteps) {
  nsteps <- 0
#   par(ask = TRUE) #Allows user to look at each transition iteratively
  while(nsteps < numSteps) {
    if(is.even(nsteps)) {
      g <- cmoveRed(g)
    } else {
      g <- cmoveBlue(g)
    }
#     plot.bml(g$grid) #Use this when asking user
    nsteps <- nsteps + 1
  }
  nsteps <- 0
#     par(ask=FALSE) #Turns this function off
  plot.bml(g$grid) #Use this when we only care about the result
  return(g)
}

summary.bml <- function(g) {
  cat("\nNumber of columns: ",ncol(g$grid))
  cat("\nNumber of rows: ",nrow(g$grid))
  cat("\nNumber of blue cars: ", g$nbluecars)
  cat("\nNumber of red cars: ",g$nredcars)
  cat("\nVelocity of red cars: " ,g$rmoved)
  cat("\nVelocity of blue cars: ",g$bmoved ) 
}

summary.density <- function(g) {
  cat("\nNumber of columns: ",ncol(g$grid))
  cat("\nNumber of rows: ",nrow(g$grid))
  cat("\nNumber of blue cars: ", g$nbluecars)
  cat("\nNumber of red cars: ",g$nredcars)
  cat("\nVelocity of red cars: " ,g$rmoved)
  cat("\nVelocity of blue cars: ",g$bmoved ) 
  cat("\nDensity: ", g$p)
}
